import Chart from "chart.js/auto";

function initChart() {
  return new Chart(ctx, {
    type: "line",
    data: {
      labels,
      datasets: [
        {
          label: "Prova",
          data,
          fill: false,
          borderColor: "rgb(75, 192, 192)",
          tension: 0.1,
        },
      ],
    },
    options: {
      animation: false,
      scales: {
        x: {
          beginAtZero: true,
          max: 100,
        },
        y: {
          beginAtZero: true,
          max: 100,
        },
      },
    },
  });
}

const ctx = document.getElementById("chart").getContext("2d");

let chart = initChart();
let data = [];
let labels = [];
