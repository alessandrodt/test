module.exports = {
   entry: {
       main: './src/main.js'
   },
   mode: "development",
   module: {
       rules: [{
          exclude: /node_modules/,
          test: /\.js$/,
          use: [
              'babel-loader',
              'prettier-loader'
            ]
       }]
   },
   output: {
        filename: '[name].bundle.js',
        path: __dirname + '/dist'
   }
};